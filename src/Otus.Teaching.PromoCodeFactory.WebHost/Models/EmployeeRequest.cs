﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.RestTypes.Administration
{
    public class EmployeeRequest
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}