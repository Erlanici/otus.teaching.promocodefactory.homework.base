﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.RestTypes.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(
            [FromBody] EmployeeRequest body
        )
        {
            var data = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = body.FirstName,
                LastName = body.LastName,
                Email = body.Email,
                AppliedPromocodesCount = body.AppliedPromocodesCount
            };
            
            var employee = await _employeeRepository.AddAsync(data);
          
            var employeeModel = new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Удалить сотрудника по id
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveEmployeeByIdAsync(Guid id)
        {
            var hasRemoved = await _employeeRepository.RemoveAsync(id);

            return hasRemoved
                ? (ActionResult) Ok()
                : NotFound();
        }
        
        /// <summary>
        /// Обновить данные по сотруднику
        /// </summary>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(
            [FromRoute] Guid id,
            [FromBody] EmployeeRequest body
        )
        {
            var data = new Employee
            {
                Id = id,
                FirstName = body.FirstName,
                LastName = body.LastName,
                Email = body.Email,
                AppliedPromocodesCount = body.AppliedPromocodesCount
            };
            
            var employee = await _employeeRepository.UpdateAsync(data);
          
            var employeeModel = new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}