﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        private static readonly object _lock = new object();

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<List<T>> GetAllAsync()
        {
            lock (_lock)
            {
                return Task.FromResult(Data);
            }
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            lock (_lock)
            {
                return Task.FromResult(Data.SingleOrDefault(x => x.Id == id));
            }
        }
        
        public Task<T> AddAsync(T data)
        {
            lock(_lock)
            {
                Data.Add(data);

                return Task.FromResult(data);
            }
        }
        
        public Task<bool> RemoveAsync(Guid id)
        {
            lock(_lock)
            {
                var result = Data.RemoveAll(item => id == item.Id);

                return Task.FromResult(result > 0);
            }
        }
        
        public Task<T> UpdateAsync(T data)
        {
            lock(_lock)
            {
                var index = Data.FindIndex(item => item.Id == data.Id);

                Data[index] = data;

                return Task.FromResult(data);
            }
        }
    }
}